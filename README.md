# We Are One : A Global Film Festival parser

## What does it do ? 

_This short scripts parses HTML pages from the official WAOFF schedule webpage (http://www.weareoneglobalfestival.com/schedule) in order to create an iCalendar-formatted file, that you can directly import in your calendar app._

Usage : 
```bash
python3 parser.py
```


**Note** : the webpage is JS-based, which means you can't just `wget` it to extract the HTML page. You'll need to browse to the schedule page you want, go to the console, and type : 
```js
console.log(document.getElementsByTagName('html')[0].innerHTML);
```
, then copy-paste it to and html file locally, that you'll browse with the script. 

